bl_info = {
    "name": "Bone Widget",
    "author": "Christophe SEUX",
    "version": (2, 0, 1),
    "blender": (3, 4, 1),
    "description": "Create custom shapes for bone controller",
    "warning": "",
    "wiki_url": "",
    "category": "Rigging",
}

import sys



if "bpy" in locals():
    import importlib as imp
    imp.reload(context)
    sys.modules.update({"bone_widget.ctx": context.BW_context()})
    
    imp.reload(shape_utils)
    imp.reload(transform_utils)
    imp.reload(icon_utils)

    imp.reload(properties)
    imp.reload(operators)
    imp.reload(ui)


else:
    from . import context
    sys.modules.update({"bone_widget.ctx": context.BW_context()})

    from . import operators
    from . import ui
    from . import properties
    from . import shape_utils
    from . import transform_utils
    from . import icon_utils

import bpy
import sys

#sys.modules.update({"bone_widget.ctx": context.BW_ctx()})



def register():
    properties.register()
    operators.register()
    ui.register()
    
    #bpy.types.Scene.bone_widget = bpy.props.PointerProperty(type=BoneWidgetSettings)
    #get_widgets(DefaultFolder, DefaultShapes)
    #get_widgets(CustomFolder, CustomShapes)
    sys.modules.update({"bone_widget.ctx": context.BW_context()})
    from bone_widget import ctx

    for f in ctx.folders:
        f.load_widgets()


def unregister():
    #print('UnRegister BoneWidget')
    
    properties.unregister()
    operators.unregister()
    ui.unregister()

    del sys.modules['bone_widget.ctx']

    #remove_icons(bpy.types.Scene.bone_widget)

